package net.rgielen.elasgfu;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author <a href="mailto:rene.gielen@gmail.com">Rene Gielen</a>
 */
public class HelloProviderTest {

    @Test
    public void testProviderDeliversSameHelloValue() throws Exception {
        assertEquals("Helo", new HelloProvider("Helo").helloVal);
    }

    @Test
    public void testDefaultProviderDeliverHello() throws Exception {
        assertEquals("Hello", new HelloProvider().helloVal);
    }

}